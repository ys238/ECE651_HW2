
public class Person {
	private int age;
	public String firstname;
	public String lastname;
	public String country;
	public String gender;
	public String degree;
	public String school;
	public String refer;
	private double height;
	private double weight;
	
	Person(String f, String l, String c, String g, String d, String s, String r){
		firstname = f;
		lastname = l;
		country = c;
		gender = g;
		degree = d;
		school = s;
		refer = r;	
	}
	public void setAge(int x) {
		age = x;
	}
	public int getAge() {
		return age;
	}
	public void setHeight(double x) {
		height = x;
	}
	public double getHeight() {
		return height;
	}
	public void setWeight(double x) {
		weight = x;
	}
	public double getWeight() {
		return weight;
	}
	public void displayPersonInformation(){	
		System.out.println("Information Dashboard");
	    System.out.println("Name : "+ firstname +' ' + lastname);
	    System.out.println("Age : "+ age);
	    System.out.println("Gender : "+ gender);
	    System.out.println("Height : "+ height + " cm");
	    System.out.println("Weight : "+ weight + " kg");
	    System.out.println("Country : "+ country);       
	 }
	public void setHobbies(String h) {
		System.out.println(refer + " likes " + h + " and often do that on weekends or after classes.");
	}
	public void setEat(String e) {
		System.out.println(refer + " usually eats " + e + " for lunch and dinner.");
	}
	public void whoIs() {
		System.out.println(firstname+' '+ lastname +" is "+ age + " years old and is "+ height + "cm tall. " + 
				refer + " was born in " + country + " and received " + degree + " degree in "+ school + '.');
		
	}
}
