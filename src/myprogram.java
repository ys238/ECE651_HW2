import java.util.*;
public class myprogram {
	
	public static void main(String[] args) {
		BlueDevil prof = new BlueDevil("Adel","Fahmy","Eastern Europe","male","master","North Carolina State University","He","af358");
		prof.setAge(40);
		prof.setHeight(176);
		prof.setWeight(54);
		BlueDevil me = new BlueDevil("Yuqin","Shen","China","female","bachelor","SCUT","She","ys238");
		me.setAge(24);
		me.setHeight(162);
		me.setWeight(48);
		BlueDevil TA1 = new BlueDevil("Yuanyuan","Yu","China","female","bachelor","ECUST","She","yy238");
		TA1.setAge(22);
		TA1.setHeight(165);
		TA1.setWeight(45);
		BlueDevil TA2 = new BlueDevil("You","Lyu","China","male","bachelor",
				"University of Nottingham Ningbo China","He","yl238");
		TA2.setAge(23);
		TA2.setHeight(172);
		TA2.setWeight(50);
		BlueDevil TA3 = new BlueDevil("Lei","Chen","China","female","bachelor","KAIST","She","lc300");
		TA3.setAge(22);
		TA3.setHeight(165);
		TA3.setWeight(45);
		BlueDevil TA4 = new BlueDevil("Zhongyu","Li","China","male","bachelor","SouthEast university","He","zl348");
		TA4.setAge(20);
		TA4.setHeight(172);
		TA4.setWeight(54);
		BlueDevil TA5 = new BlueDevil("Shalin","Shah","India","female","bachelor","DA-IICT","She","ss348");
		TA5.setAge(23);
		TA5.setHeight(165);
		TA5.setWeight(45);
		String a;
	      Scanner in = new Scanner(System.in);
	      System.out.println("Please EXPAND the output window first because I have 10+ lines output for each name.");
	     do { String s; 
	      System.out.println("We have 8 persons information stored: Yuqin Shen(me), Adel Fahmy, Yuanyuan Yu, You Lyu, "
	      		+ "Zhongyu Li, Lei Chen, Shalin Shah, Sihao Yao");
	      System.out.println("Please type the name you would like to know. CASE SENTITIVE! Some information is fabricated!");
	      s = in.nextLine();
	      switch(s) {  
	      case "Yuqin Shen":
	    	  System.out.println("Hello! It is me! I would appreciate if you give me 50 points! Haha~");
	    	  me.displayPersonInformation();
	    	  me.whoIs();
	    	  me.setHobbies("yoga, swinging and cooking");
	    	  me.setEat("Guangdong food and soap");
	    	  me.setMajor("Electrical and Computer Engineering");
	    	  me.getContact();
	    	  break;
	      case "Yuanyuan Yu":
	    	  TA1.displayPersonInformation();
	    	  TA1.whoIs();
	    	  TA1.setHobbies("baseball and fencing");
	    	  TA1.setEat("Chinese food");
	    	  TA1.setTA("ECE651");
	    	  TA1.getContact();
	    	  break;
	      case "You Lyu":
	    	  TA2.displayPersonInformation();
	    	  TA2.whoIs();
	    	  TA2.setHobbies("traveling, music and history");
	    	  TA2.setEat("Chinese food");
	    	  TA2.setTA("ECE651");
	    	  TA2.getContact();
	    	  break;
	      case "Lei Chen":
	    	  TA3.displayPersonInformation();
	    	  TA3.whoIs();
	    	  TA3.setHobbies("climbing and animals");
	    	  TA3.setEat("Chinese food");
	    	  TA3.setTA("ECE651");
	    	  TA3.getContact();
	    	  break;
	      case "Zhongyu Li":
	    	  TA4.displayPersonInformation();
	    	  TA4.whoIs();
	    	  TA4.setHobbies("basketball & NBA");
	    	  TA4.setEat("Chinese food");
	    	  TA4.setTA("ECE651");
	    	  TA4.getContact();
	    	  break;
	      case "Shalin Shah":
	    	  TA5.displayPersonInformation();
	    	  TA5.whoIs();
	    	  TA5.setHobbies("bodybuilding and dancing");
	    	  TA5.setEat("Indian food");
	    	  TA5.setTA("ECE651");
	    	  TA5.getContact();
	    	  break;
	      case "Adel Fahmy":
	    	  prof.displayPersonInformation();
	    	  prof.whoIs();
	    	  prof.setHobbies("tennis and reading");
	    	  prof.setEat("local food");
	    	  prof.setTeach("ECE651 Software Engineering");
	    	  prof.getContact();
	    	  break;
	      case "Sihao Yao":
	    	  System.out.println("Sorry, there is no detail information for this person on the TAs introduction slide!");
	    	  break;
	     default:
	    		  System.out.println("Name typo or no such person information!");
	      }
	      System.out.println("**************************************************************************************");
	      System.out.println("Continue(Y/N)");
	     a = in.nextLine();
	      }while(a.contentEquals("Y"));
	     if(!a.contentEquals("N")){
	     System.out.println("Wrong input! Please rerun the program to retrieve information!");
	  }
	}

}
