# ECE651_HW2

HW2 submission

It is a Java program which gets input from **stdin**.
Run **myprogram**, and you will be asked to type the name of the person you would like to ask
about. Follow the instructions, you will retrieve the person's information. I used ```whoIs()``` functhon inside of
the Person class definition.

**src should have 6 files:**
* BlueDevil.class
* BlueDevil.java  
* myprogram.class  
* myprogram.java  
* Person.class  
* Person.java

## Running for Windows
1. The code has been **compiled**. To run this code, download all src files to local directory. 
2. Open Command Prompt/Terminal.
3. Navigate to the correct folder.
4. Enter in ```java myprogram``` , and click Enter on the keyboard.
5. Follow the instructions of the program shown on the screen.
